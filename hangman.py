__author__ = 'trzepka'
import random

class Hangman():

    words_file = "words"
    max_words = 1
    word = "a"

    max_tries = 6
    tries = 0
    guessed = set()

    def __init__(self):
        self.get_words_count()

    def get_words_count(self):
        with open(self.words_file) as f:
            for i, l in enumerate(f):
                if not l.strip("\n").isalpha():
                    print(l.strip("\n"))
                    raise Exception("Word file is not [A-Za-z]")
        self.max_words = i+1

    def get_new_word(self):
        word_number = random.randrange(self.max_words)
        fp = open(self.words_file)
        for i, line in enumerate(fp):
            if i == word_number:
                self.word = line.strip("\n").lower()
            elif i > word_number:
                break
        fp.close()

    def guess(self, guess):
        if guess in self.guessed:
            print("Try something new")
        else:
            if guess in self.word:
                self.guessed.add(guess)
            else:
                self.tries += 1
                self.guessed.add(guess)
        print("guesses: ["+str(self.tries)+"/"+str(self.max_tries)+"] {"+", ".join(str(e) for e in self.guessed)+"}")

    def print(self):
        for letter in self.word:
            if letter in self.guessed:
                print(letter,end=" ")
            else:
                print("_",end=" ")
        print("\n^^^^^^^^^^^^^^^^^^^^^^^^^^^")

    def play(self):
        a.get_new_word()
        self.tries = 0
        self.guessed=set()

        while self.tries < self.max_tries:
            guess = input('Type your guess: ').lower()
            if not guess.isalpha():
                print("only letters are allowed")
                continue
            if len(guess) != 1:
                print("type only one letter")
                continue
            print("vvvvvvvvvvvvvvvvvvvvvvvvvvv")
            self.guess(guess)
            self.print()

            if set(self.word) <= self.guessed:
                print("you won")
                return

        print("you lost ("+self.word+")")

a = Hangman()
a.play()